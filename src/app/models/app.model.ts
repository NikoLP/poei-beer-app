export interface Beer {
  id: number;
  name: string;
  tagline: string;
  description: string;
  first_brewed: string;
  image_url: string;
  ibu: string;
  brewers_tips: string;
}
