import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiBeerComponent } from './ui-beer.component';

describe('UiBeerComponent', () => {
  let component: UiBeerComponent;
  let fixture: ComponentFixture<UiBeerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiBeerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiBeerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
