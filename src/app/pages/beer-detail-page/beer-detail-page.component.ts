import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Beer } from '@app/models/app.model';
import { BeerService } from '@app/services/beer.service';

@Component({
  selector: 'app-beer-detail-page',
  templateUrl: './beer-detail-page.component.html',
  styleUrls: ['./beer-detail-page.component.scss'],
})
export class BeerDetailPageComponent implements OnInit {
  private beerId?: number;
  public beer?: Beer;

  constructor(
    private activatedRoute: ActivatedRoute,
    private beerService: BeerService,
    private router: Router
  ) {
    this.beerId = parseInt(this.activatedRoute.snapshot.params['id'], 10);

    const beer = this.beerService.beers.find((b) => b.id === this.beerId);

    if (!beer) {
      return;
    }

    this.beer = beer;
  }

  async ngOnInit() {
    if (!this.beer && this.beerId) {
      try {
        this.beer = await this.beerService.getById(this.beerId);
      } catch (error) {
        this.router.navigate(['/']);
      }
    }
  }
}
