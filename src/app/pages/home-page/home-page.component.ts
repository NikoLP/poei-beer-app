import { Component, OnInit } from '@angular/core';
import { BeerService } from '@app/services/beer.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  public title: string = 'Catalogue de bières';
  public isInitialLoading = false;

  constructor(public beerService: BeerService) {}

  async ngOnInit() {
    if (!this.beerService.beers.length) {
      this.isInitialLoading = true;
      await this.beerService.loadBeers();
      this.isInitialLoading = false;
    }
  }
}
