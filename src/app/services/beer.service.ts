import { Injectable } from '@angular/core';
import { Beer } from '@app/models/app.model';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})
export class BeerService {
  private apiUrl: string = 'https://api.punkapi.com/v2/beers';
  public beers: Beer[] = [];

  public async getById(id: number): Promise<Beer> {
    const { data } = await axios.get(`${this.apiUrl}/${id}`);
    return data.map((beer: any) => {
      return {
        id: beer.id,
        name: beer.name,
        tagline: beer.tagline,
        description: beer.description,
        first_brewed: beer.first_brewed,
        image_url: beer.image_url,
        ibu: beer.ibu,
        brewers_tips: beer.brewers_tips,
      };
    })[0];
  }

  public async fetch(): Promise<Beer[]> {
    const { data } = await axios.get(`${this.apiUrl}`);
    return data.map((beer: any) => {
      return {
        id: beer.id,
        name: beer.name,
        tagline: beer.tagline,
        description: beer.description,
        first_brewed: beer.first_brewed,
        image_url: beer.image_url,
        ibu: beer.ibu,
        brewers_tips: beer.brewers_tips,
      };
    });
  }

  public async loadBeers() {
    this.beers = await this.fetch();
  }
}
